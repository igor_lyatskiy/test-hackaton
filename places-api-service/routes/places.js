const express = require('express');
const router = express.Router();
const faker = require('faker');
const chance = new require('chance').Chance();
const jsf = require('json-schema-faker');
jsf.extend('chance', () => chance);
jsf.extend('faker', () => faker);


var schema = {
	"type": "array",
	"minItems": 1,
	"maxItems": 1,
	"items":{
		type:'object',
		properties: {
			 streetName: {
		          type: 'string',
		          faker: 'address.streetAddress'
       		 },
       		 streetAddress: {
		          type: 'string',
		          faker: 'address.secondaryAddress'
       		 },
       		 city: {
       		 	type:'string',
       		 	chance: 'city'
       		 }
		},
		 required: ['streetName', 'streetAddress', 'city']
	}
};


/* GET users listing. */
router.get('/', (req, res) => {

	jsf.resolve(schema).then(sample => {
	  res.send(sample);
	});
});

module.exports = router;
