const express = require('express');
const router = express.Router();
const faker = require('faker');
const chance = new require('chance').Chance();
const jsf=require('json-schema-faker');
jsf.extend('chance',()=>chance);
jsf.extend('faker',()=>faker );


var schema = {
	"type": "array",
	"minItems":5,
	"maxItems":7,
	"items":{
		type:'object',
		properties: {
			city: {
          		type: 'string',
          		faker: 'address.city'
			},
			toy_name: {
				type: 'string',
				faker: 'commerce.productName'
			},
			cost: {
				type: 'integer',
				minimum: 10,
				maximum: 60
			}
		},
		required: ['city', 'toy_name', 'cost']
	}
};


/* GET users listing. */
router.get('/', (req, res) => {
	jsf.resolve(schema).then(sample => {
    res.send(sample);
	});
});

module.exports = router;
